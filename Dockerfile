FROM quay.io/prometheus/golang-builder AS builder

ADD .   /go/src/gitlab.com/njohnstone/cmc_exporter
WORKDIR /go/src/gitlab.com/njohnstone/cmc_exporter

RUN make

FROM        quay.io/prometheus/busybox:glibc
LABEL maintainer="The Prometheus Authors <prometheus-developers@googlegroups.com>"
COPY        --from=builder /go/src/gitlab.com/njohnstone/cmc_exporter/cmc_exporter  /bin/cmc_exporter

EXPOSE      9599
ENTRYPOINT  [ "/bin/cmc_exporter" ]